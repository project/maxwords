Maxwords

Description:
This module allows setting a minimum and maximum number of words for Long Text fields.
Further field types will be supported in the future.

Similar modules:
- Maxlength: Maxlength is counting the number of characters.
- Field validation: A comprehensive collection of field validators including a word counter.